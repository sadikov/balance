<?php

namespace Database\Seeders;

use App\Models\BalanceHistory;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BalanceHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startDate = Carbon::createFromDate('2021', '1', '1');
        $userBalance = [];
        for ($day = 0; $day <= 100; ++$day) {
            for ($userId = 1; $userId <= 20; ++$userId) {
                if ($day == 0) {
                    $value = rand(0, 1000);
                } else {
                    $value = rand(-1000, 1000);
                }
                $userBalance[$userId] = isset($userBalance[$userId])
                    ? $userBalance[$userId] + $value
                    : $value;
                BalanceHistory::create([
                    'user_id' => $userId,
                    'value' => $value,
                    'balance' => $userBalance[$userId],
                    'created_at' => $startDate->copy()->addDays($day)->toDateTimeString()
                ]);
            }
        }
    }
}
