<?php

declare(strict_types=1);

namespace App\Http\Procedures;

use App\Models\BalanceHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Sajya\Server\Procedure;

class UserBalanceProcedure extends Procedure
{
    /**
     * The name of the procedure that will be
     * displayed and taken into account in the search
     *
     * @var string
     */
    public static string $name = 'balance';

    public function userBalance(Request $request) : float
    {
        $userId = $request->input('user_id');
        $dbItem = BalanceHistory::where('user_id', '=', $userId)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $dbItem ? $dbItem->balance : 0;
    }

    public function history(Request $request) : Collection
    {
        $limit = $request->input('limit');
        return BalanceHistory::query()
            ->orderBy('created_at', 'DESC')
            ->limit($limit)
            ->get();
    }
}
